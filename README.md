## CKEditor 5 Dark Mode Plugin for Drupal

This module is a plugin in addition to CKEditor5,
simply allowing to set up a Dark Mode button to switch
between light theme and dark theme.
Some CSS properties have been added to workaround few issues
with Gin theme colors.
