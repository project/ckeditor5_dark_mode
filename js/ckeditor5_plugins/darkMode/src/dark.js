import DarkUI from './darkui';
import {
  Plugin
} from 'ckeditor5/src/core';

export default class Dark extends Plugin {

  static get requires() {
    return [DarkUI];
  }
}
