import { Plugin } from 'ckeditor5/src/core';

import {
  ButtonView
} from 'ckeditor5/src/ui';

import iconDark from '../../../../icons/dark.svg';
import iconLight from '../../../../icons/light.svg';

export default class DarkUI extends Plugin {

  init() {
    const editor = this.editor;
    const darkClass = 'ck-dark';

    // This will register the fullscreen toolbar button.
    editor.ui.componentFactory.add('darkMode', locale => {
      const buttonView = new ButtonView(locale);
      const editorRegion = editor.sourceElement.nextElementSibling;

      let state = 0;
      // Callback executed once the image is clicked.
      buttonView.set({
        label: 'Dark Mode',
        icon: iconDark,
        tooltip: true
      });

      buttonView.on('execute', () => {
        if (state == 1) {

          editorRegion.classList.remove(darkClass);

          buttonView.set({
            label: 'Dark Mode',
            icon: iconDark,
            tooltip: true
          });
          state = 0;

        } else {
          editorRegion.classList.add(darkClass);

          buttonView.set({
            label: 'Light Mode',
            icon: iconLight,
            tooltip: true
          });
          state = 1;
        }
      });
      return buttonView;
    });
  }
}

